using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header("Refrences")]
    public GameObject projectile;
    public Transform firePoint;

    [Header("Settings")]
    public float speed; //projectile speend
    public float shootRate; //rate of fire
    public int curAmmo; //current ammo
    public int maxAmmo; // max ammo
    //change ammo values to be attached to player. "Power" will act as ammo and player health. costs power to fire, but gains power back whem am ememy is hit

    [Header("Debug")]
    public bool infAmmo; //infinite ammo 
    private bool isPlayer; //is attached to player?
    private float lastShootTime; //last time shot

    private void Awake() {
        // are we attached to the player
        if(GetComponent<PlayerControler>())
            isPlayer = true;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CanShoot(){
        if(Time.time - lastShootTime >= shootRate || shootRate == 0){
            if(curAmmo > 0 || infAmmo)
                return true;
        }

        return false;
    }

    public void Shoot(){
        lastShootTime = Time.time;
        curAmmo--;

        GameObject projectileObject = Instantiate(projectile, firePoint.position, firePoint.rotation);
        projectileObject.GetComponent<Rigidbody>().velocity = firePoint.forward * speed;
    }
}
