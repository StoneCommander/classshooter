using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlag : MonoBehaviour
{
    private GameManager gm;
    private Renderer flagRend;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        flagRend = GetComponent<Renderer>();

        flagRend.enabled = true;
        
    }

    void OnCollisionEnter(Collision other){
        gm.hasFlag = true;
        flagRend.enabled = false;
    }
}
