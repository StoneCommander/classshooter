using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    [Header("Movement Info")]
    public float moveSpeed;
    public float jumpForce;
    
    [Header("Camera Info")]
    public float lookSensitivity;
    public float maxLookX; //max down
    public float minLookX; //max up
    private float rotX; //camera rot (vert)
    private Camera cam;
    private Rigidbody rb;
    private Projectile weapon;

    void Awake() {
        //get game components
        cam = Camera.main;
        rb = GetComponent<Rigidbody>();
        weapon = GetComponent<Projectile>();

        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        CamLook();
        if(Input.GetButtonDown("Jump"))
            TryJump();
        if(Input.GetButton("Fire1") && weapon.CanShoot())
            weapon.Shoot();
    }

    void Move() {
        float x = Input.GetAxis("Horizontal") * moveSpeed; // Left <=> Right
        float z = Input.GetAxis("Vertical") * moveSpeed; // Forward <=> Back
        
        // move to camera
        Vector3 dir = transform.right * x + transform.forward * z;
        dir.y = rb.velocity.y;

        rb.velocity = dir;
    }

    void CamLook(){
        float y = Input.GetAxis("Mouse X") * lookSensitivity;
        rotX += Input.GetAxis("Mouse Y") * lookSensitivity;

        rotX = Mathf.Clamp(rotX, minLookX, maxLookX);

        cam.transform.localRotation = Quaternion.Euler(-rotX, 0, 0);
        transform.eulerAngles += Vector3.up * y;
    }

    void TryJump(){
        Ray ray = new Ray(transform.position, Vector3.down);

        if(Physics.Raycast(ray, 1.1f))rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }
}
